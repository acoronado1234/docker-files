# PostgreSQL

This docker-compose creates a PostgreSQL server on port 5432 which can be
interacted with using the Adminer front end on port 8080.
